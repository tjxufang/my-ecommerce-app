import React from 'react';
import './productList.css'
import {connect} from "react-redux";
import {actAddCart, actFetchProduct} from "../actions";

class ProductList extends React.Component {

    componentDidMount() {
        this.props.actFetchProduct()
    }

    async handleClick(product) {
        let totalPrice = product.price
        this.props.actAddCart(product, 1, totalPrice)
    }

    renderProductList() {
        const productList = this.props.products
        console.log(productList);
        console.log(this.props.cartItems)
        return productList?.map((product, i) => {
            return (
                <div key={i} className="singleProduct">
                    <img src={product.media.split("|")[0]} alt="product" style={{width: '125px'}}/>
                    <br/>
                    <div>
                        <h2>{product.name}</h2>
                        <div style={{display: 'flex', flexWrap: 'wrap', justifyContent: 'space-between'}}>
                            <h3>${product.price}</h3>
                            <button onClick={() => this.handleClick(product)}>Add to Cart</button>
                        </div>
                    </div>
                    {product.description}
                </div>
            )
        })
    }

    render() {
        return (
            <div className="listContainer">
                {this.renderProductList()}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        products: state.fetchProductReducer,
        cartItems: state.cartReducer.cartItems
    }
}

const mapDispatchToProps = {actFetchProduct, actAddCart}


export default connect(mapStateToProps, mapDispatchToProps)(ProductList)