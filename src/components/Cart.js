import React from 'react';
import './cart.css'
import {connect} from "react-redux";
import {actAddCart, actRemoveItem, actSubtractOne} from "../actions";

class Cart extends React.Component {
    addItem = (item, quantity, totalPrice) => {
        this.props.actAddCart(item, 1, totalPrice)
        console.log('item added')
    }

    subItem = (item, quantity, totalPrice) => {
        this.props.actSubtractOne(item, quantity, totalPrice)
        console.log('item subtracted')
    }


    removeItem = item => {
        this.props.actRemoveItem(item)
        console.log('item removed', item)
    }


    render() {
        let subTotal = (this.props.cartItems.reduce((counter, item) => counter + parseInt(item.totalPrice), 0)).toFixed(2) // output as a string after .toFixed
        let taxes = (subTotal * 0.13).toFixed(2)
        let total = parseFloat(subTotal) + parseFloat(taxes)

        return (
            <div className="shoppingCart">
                <button onClick={() => this.props.history.push('/')}>Go back</button>
                <div className="cartContainer">
                    <div className="cartContent">
                        {this.props.cartItems.map((item, i) => {
                            return (
                                <div key={i} className="singleItem">
                                    <img src={item.media.split("|")[0]} alt="item" style={{width: '100px'}}/>
                                    <div className="singleItemDescription">
                                        <div className="itemDetails" style={{width: '30%'}}>Item: {item.name}</div>
                                        <div className="itemDetails" style={{width: '20%'}}>
                                            <div>Quantity: {item.quantity}</div>
                                            <button className="editQuantity" onClick={() => this.addItem(item, 1, item.price)}>+1</button>
                                            <button className="editQuantity" onClick={() => this.subItem(item, 1, item.price)}>-1</button>
                                        </div>
                                        <button onClick={() => this.removeItem(item)}>Remove All</button>
                                        <div className="itemDetails" style={{width: '20%'}}>Price:
                                            ${item.totalPrice}</div>
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                </div>
                <div className="checkout">
                    <div>Subtotal: ${subTotal}</div>
                    <br/>
                    <div>taxes: ${taxes}</div>
                    <br/>
                    <div>total: ${total}</div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        cartItems: state.cartReducer.cartItems,
    }
}

const mapDispatchToProps = {
    actRemoveItem, actAddCart, actSubtractOne
}


export default connect(mapStateToProps, mapDispatchToProps)(Cart)