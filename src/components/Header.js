import React from 'react';
import logo from "../assets/logo192.png";
import './header.css'
import {connect} from "react-redux";
import {Link} from "react-router-dom";

class Header extends React.Component {

    render() {
        return (
            <div className="headerContainer">
                <div className="header">
                    <div className="logo">
                        <Link to={'/'}>
                            <img src={logo} alt="logo" width='45px'/>
                            <p style={{display: 'inline-block'}}>eCom</p>
                        </Link>
                    </div>
                    {this.props.location.pathname === "/cart" ?
                        <div className="cart">
                            <p>Thank you for shopping!</p>
                        </div>
                        :
                        <div className="cart">
                            <Link to={"/cart"}>
                                <img
                                    src="https://img.favpng.com/3/9/19/shopping-cart-logo-shopping-bags-trolleys-png-favpng-h2qvxF41UbVd23Kk2iTKZtCug.jpg"
                                    alt="cart" width="30px"/>
                                <p style={{display: 'inline-block'}}>Shopping Cart ({this.props.cartQuantity})</p>
                            </Link>
                        </div>
                    }
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        cartQuantity: state.cartReducer.cartItems.reduce((counter, item) => counter + parseInt(item.quantity), 0)
    }
}

export default connect(mapStateToProps,{})(Header)
