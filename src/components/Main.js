import React from 'react';
import SideNav from "./SideNav";
import './main.css'
import ProductList from "./ProductList";

function Main(props) {
    return (
        <div className="section">
            <div className="sideNav">
                <SideNav/>
            </div>
            <div className="productList">
                <ProductList/>
            </div>
        </div>
    );
}

export default Main;