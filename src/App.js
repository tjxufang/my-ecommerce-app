import './app.css'
import Main from "./components/Main";
import Header from "./components/Header";
import {BrowserRouter, Route} from "react-router-dom";
import Cart from "./components/Cart";

function App() {
    return (
        <div className="layout">
            <BrowserRouter>
                <Route component={Header}/>
                <div className="mainContent">
                    <Route path="/" exact component={Main}/>
                    <Route path="/cart" exact component={Cart}/>
                </div>
            </BrowserRouter>
        </div>
    )
}

export default App;
