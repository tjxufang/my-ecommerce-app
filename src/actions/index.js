import ecomProduct from "../apis/ecomProduct";

export const actFetchProduct = () => (
    async dispatch => {
        try {
            let products = await ecomProduct.get('product')
            dispatch({
                type: "FETCH_PRODUCTS",
                payload: products
            })
        } catch (e) {
            console.log("fetching product failed", e)
        }
    }
)

export const actAddCart = (product, quantity, totalPrice) => async dispatch => {
    dispatch({
        type: "ADD_CART",
        payload: {...product, quantity, totalPrice}
    })
}

export const actSubtractOne = (product, quantity, totalPrice) => async dispatch => {
    dispatch({
        type: "SUB_ONE",
        payload: {...product, quantity, totalPrice}
    })
}

export const actRemoveItem = (product) => async dispatch => {
    dispatch({
        type: "DELETE_ALL",
        payload: {...product}
    })
}