import {combineReducers} from "redux"

const initState = {
    cartItems: [],
}

const fetchProductReducer = (state = [], action) => {
    if (action.type === "FETCH_PRODUCTS") {
        return action.payload.data.data
    }
    return state
}

const cartReducer = (state = initState, action) => { // action.payload = new item to be added
    switch (action.type) {
        case "ADD_CART":
            if (state.cartItems.find(item => item.id === action.payload.id)) { // if previously added
                let tempCart = state.cartItems.map(item => item.id === action.payload.id ? // to find the matching item
                    { // found the item to be updated: combine 2 duplicates by fixing quantity and totalPrice
                        ...item, // keep the original item info
                        quantity: parseInt(item.quantity) + parseInt(action.payload.quantity),
                        totalPrice: (parseInt(item.quantity) + parseInt(action.payload.quantity)) * item.price
                    }
                    :
                    {...item}) // not the matching item to be updated
                return {...state, cartItems: tempCart}
            } else { // if not previously added
                return {...state, cartItems: [...state.cartItems, action.payload]} // simply add the new item
            }
        case "SUB_ONE":
            if (parseInt(state.cartItems[action.payload.id-1].quantity) === 1){
                console.log("to 0")
                let tempCart = state.cartItems.filter(item => item.id !== action.payload.id) // keep items that are not selected
                return {...state, cartItems: tempCart}
            } else {
                console.log("not to 0")
                let tempCart = state.cartItems.map(item => item.id === action.payload.id ? // if finds the duplicate
                    {
                        ...item,
                        quantity: parseInt(item.quantity) - parseInt(action.payload.quantity),
                        totalPrice: (parseInt(item.quantity) - parseInt(action.payload.quantity)) * item.price
                    }
                    :
                    {...item})
                return {...state, cartItems: tempCart}
            }
        case "DELETE_ALL":
            let tempCart = state.cartItems.filter(item => item.id !== action.payload.id) // keep items that are not selected
            return {...state, cartItems: tempCart}
        default:
            return state
    }
}

export default combineReducers({
    fetchProductReducer,
    cartReducer,
})